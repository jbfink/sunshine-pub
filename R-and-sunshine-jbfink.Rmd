---
title: "Learning to Program: R, The 2018 Ontario Sunshine List and Librarians"
output:
  pdf_document: default
  word_document: default
  html_document:
    df_print: paged
---
```{r bootstrap, include = FALSE}
# Congratulations on viewing the source Markdown! This should never evaluate for the print version:
# "bootstrap.R" merely sets up all the variables, functions, and other associated bric-a-brac to make
# the rest of the document work.
source("bootstrap.R")
```

## About the author
Hello. I'm John Fink, the Digital Scholarship Librarian at McMaster University. My background is in library systems administration, and I have made several attempts over the years to learn programming that have always petered out. Just about every time I run into something I like to call the Owl Problem.

![](./pub/images/owl-problem.png)

The owl problem is this: you start a tutorial to a language. The authors, being experienced programmers themselves, know that you have to start with the basics when teaching programming. Consequently, they will do something like this: "Language X is very easy! Let's start by doing a simple calculation! Here, add 2 + 2!"
```{r two plus two}
2+2
```

So you do this, and it comes out 4, and you get really excited. "Wow", you think. "Programming is easy and fun. Let's keep going!" The rest of the first chapter is similar, you print some stuff, you do some more calculations, you're humming along and you feel terrific. Then comes Chapter 2 and Chapter 2 hits you with something like:

```{brainfuck a brainfuck example}
++++++++++[>+++++++<-]>--.<+++++++[>++++++<-]>+.----------.++++++++++++++.<+++++++++[>---------<-]>--.<+++
+++++++++[>+++++++<-]>.------------.-------.<+++++[>++++<-]>-.<++++++++++++[>-------<-]>.<+++++++++[>++++++
+++<-]>+.-------------.----.+++++++++++..+++++++++++++.<++++++++++[>---------<-]>+.<+++++++++++[>++++++<-]>
+.++++++++++++.++++++.-------.++++++.<++++++++++++[>-------<-]>.<++++++++[>++++++++<-]>+.<++++++[>+++<-]>.<
+++++++++[>---------<-]>--.<++++++++[>++++++++<-]>+.<++++++++[>--------<-]>-.<++++++++++[>++++++++<-]>.++.-
--..---------.<++++++++[>-------<-]>--.------------.<++++++++++++[>+++++++<-]>.------------.+++++++.++++++.
--------------.+.<+++++++[>------<-]>+.?
```

Note: this is actual working code -- and in fact, evaluates to the same thing as the ```2+2``` code above -- in a language called [Brainfuck](https://en.wikipedia.org/wiki/Brainfuck). Brainfuck is Turing complete, which means it can be used for things that most normal programming languages can be used for, but it is emphatically *not* a normal language; it is, like a lot of nerd humour, an over-engineered joke.

I have seen this happen *over and over* again with language tutorials. I can't fault the intentions of authors -- it's rough business to write a tutorial -- but I think their *remove* from the beginner mind is a real detriment to effective teaching. They know the fundamentals and they know the complex possibilities that can be derived from fundamentals, but they're all-to-often missing the middle step.

## Why R
### Ruby and DSLs
Back when I was plunging headlong into learning [Ruby](https://www.ruby-lang.org/en/) and making slow progress, I encountered something that is very common within Ruby but of which I had never heard -- the concept of the Domain Specific Language, or DSL.  [DSL](https://thoughtbot.com/blog/writing-a-domain-specific-language-in-ruby) is a language that is *intentionally restricted* so that it is more suited to a specific application. You can think of [Twitter](https://twitter.com) as a kind of Web DSL -- it would be trivially easy to, say, remove the 500 character limit on individual tweets and just allow people an unlimited number of characters, but to do so would be to transform Twitter into something it is not. In a similar vein, the popular [Ruby on Rails](https://rubyonrails.org) framework is a Ruby DSL that is expressly intended for building web applications. Its syntax is Ruby, it depends on Ruby to work, and yet it both is and is not Ruby.

### The role of IDEs and REPLs
### The R ecosystem

## The Sunshine List
### List history
### Why the list
### The list sucks, but why
```{r me}
me[, c(2,3,4,6,7)]
```