### Divide greater set into universities, and then into universities and librarians (done)

# basic qs:

## How many people at McMaster University are on the sunshine list?

* Two ways:
* `sum(regexpr("McMaster University",sunshine2018$Employer) != -1)`
* `sum(grepl("McMaster University", sunshine2018$Employer))`

## Who is the most highly paid person at McMaster University?

* Sort -- `sort(mcmaster$Salary.Paid, decreasing = TRUE)`

## How to pick row based on sort, display entire row?

* `mcmaster[which.max(mcmaster$Salary.Paid), ]`

## How to pick arbitrary number of top salaries? (e.g., 10)

* `head(arrange(librarians,desc(Salary.Paid)), n = 10)` (from `https://stackoverflow.com/questions/12187891/how-to-get-top-n-companies-from-a-data-frame-in-decreasing-order`

## Find every librarian at York in sunshine, sorted by descending, printing all of the tibble:

* `librarians %>% filter(Employer == "York University") %>% arrange(desc(Salary.Paid)) %>% print(n=Inf)` 
Note that, at least with pipes, arrange() is better than sort().


## What is the median salary for librarians across institutions? Plot thusly:

x-axis individual university

y-axis median salary


## To get median salary for arbitrary institution: 

* `median((librarians %>% filter(Employer == "McMaster University"))[[4]])`
note that the [[]] is necessary! [4] returns a tibble, [[4]] returns a numeric. This is vaguely familiar to you,
so maybe look it up a bit. You got clued in by `https://stackoverflow.com/questions/43353228/median-need-numeric-data`

### WRITE A FUNCTION FOR THIS! so like, my_func <- function(dataset, institution) { median(dataset %>% filter(Employer == institution))[[4]]. Optionally have it spit out a tiny dataframe or tibble or something. 

### How to write a beginning lapply to spit out medians:
* `lapply(list_of_universities, median_pay, dataset = librarians)`

### Here's how to spit out a nice named tibble of medians!:
* `uni_tibble <- enframe(sapply(list_of_universities, median_pay, dataset = universities))`
* figure out how to use something (mutate()?) to A) rename columns and B) add a column with librarian equivalents.
* DONE: `add_column(uni_tibble, library = as_vector((lapply(list_of_universities, median_pay, dataset = librarians))))`

### Pulling every university and putting into a vector:
* `uni_vec <- unique(pull(universities, Employer))`

### and a graph:
* `ggplot(data = (enframe(sapply(list_of_universities, median_pay, dataset = universities))), aes(name, value)) + geom_point()`
### How to create tibble from a function - 

`https://tibble.tidyverse.org/reference/tribble.html`

### How to add row to existing tibble -

`https://tibble.tidyverse.org/reference/add_row.html` 


# medium extra credit:

## Add column to librarians tibble for affiliation (MUALA, TMG, MUFA etc)
* `librarians <- add_column(librarians, Affiliation = NA)`
* will have to add affiliation manually. :(
* How to find Janice's affiliation -- `(librarians %>% filter(librarians$Employer == "McMaster University"))[1,9 ]`
* Replace value with `replace()`, `recode()`, `mutate()`, other ???
* UGh so close. This makes a new tibble with the first five people at Mac designated MUALA. :/ `librarians2 <- (librarians %>% filter(librarians$Employer == "McMaster University"))[1:5,  ] %>% mutate(Affiliation = "MUALA")`
* HOW TO EDIT AND CHANGE IN PLACE ENTIRE TIBBLE RATHER THAN MAKING NEW SUBSET TIBBLE???
* May have to create new function? `https://stackoverflow.com/questions/34096162/dplyr-mutate-replace-on-a-subset-of-rows`
* magrittr required, but this adds MUALA affiliation to entire subset of McMaster librarians:
* `librarians[librarians$Employer == "McMaster University", ] %<>% mutate(Affiliation = "MUALA"`
* Change Vivian to TMG: `librarians[(librarians$Last.Name == "Lewis" & librarians$First.Name == "Vivian"), ] %<>% mutate(Affiliation = "TMG")`
* Write a function to do the above -- why doesn't the below work??
` function(dataset = librarians, lastname = "Fink", firstname = "John", affil = "AWESOME") {
    dataset[(dataset$Last.Name == lastname & dataset$First.Name == firstname), ] %<>% mutate(Affiliation = affil)
}`
* It doesn't work 'cos scope. Inside a function is a temporary scope, outside the function is global scope. This can be fixed with the <<- operator, but we're not using an assignment operator inside the function (right now, may have to fix).
* Maybe do the assignment outside the function, so that function returns a value and use <- to assign? ala `https://stackoverflow.com/questions/3969852/update-data-frame-via-function-doesnt-work`
* Have just about decided to stuff all this shit and just do it with direct calls to rows, e.g., to make Wade Wyckoff a MUFA member, we do `librarians[c(68), c(2,9)]$Affiliation = "MUFA"`. 

* Find *total* librarian employment for each institution (ARL sheet?) and then plot how many sunshine librarians are there vs. total pop

* Find median salary for university gen pop, plot that against median salary for librarians

## Super extra credit:

Take all historical sheets, mean them throughout time?

## AT SOME POINT:

Write up a boostrap.R to take the INITIAL CSV and create all the applicable dataframes/csvs. e.g., convert money to decimals,
create universities/libraries tibbles, add (and populate?) affiliation column, etc.

## WTF

Somehow in librarians tibble Salary.Paid and Taxable.Benefits became... un-numeric (characters, in quotes). Why??? Anyway, fixed via something like `librarians$Taxable.Benefits <- as.double(librarians$Taxable.Benefits)` wtf

## create new small tibble of median pay
Use a lappy/sapply/vapply/tapply/apply (wtf is the difference anyway) with the median_pay function to make a tibble with university, median pay
* Here, kinda! 
* 'medians <- lapply(uni_vec, median_pay)`
* `names(medians) <- uni_vec`
* `melt(medians)` -- requires library(reshape). Also figure out why the columns are reversed?
* `medians <- melt(medians)[,c(2,1)]`
* remove NAs from medians `medians <- medians[!is.na(medians$value), ]`

## STRIP LIBRARIANS OF EVERY INSTITUTION WHERE THERE IS ONLY ONE ENTRY
Freaking UOIT/etc are skewing numnbers. Alternately, add all the affiliations and exclude management.
